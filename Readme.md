# Assessment Week 1

kerjakan project freecodecamp yang ada di [link ini](https://www.freecodecamp.org/learn/responsive-web-design/#responsive-web-design-projects)


1. Jelaskan peran kita sebagai front-end web developer/front-end engineer!
>Peran Frontend Developer adalah mentranslate design murni dari seorang UI/UX designer untuk dijadikan suatu produk yang interaktif dengan user baik itu berupa website maupun aplikasi.
---

2. hardskill apa saja yang minimal harus dimiliki oleh seorang front-end webd developer/front-end engineer?
>1. HTML & CSS
>1. Javascript
>1. Framework CSS dan JavaScript
>1. Prepocessor CSS
>1. Responsive
>1. Command Line
>1. Version Control System (Git)
>1. Debugging
---

3. kerjakan project pertama "Build a Tribute Page",
jika sebelum assesment ini diberikan teman-teman sudah menyelesaikan project tersebut
silahkan kerjakan project kedua, jika project kedua juga sudah selesai sebelum
assesment ini diberikan, kerjakan project ketiga dan seterusnya.
untuk sementara silahkan gunakan template codepen yang sudah di sediakan oleh freecodecamp.
>+ Build a Tribute Page :heavy_check_mark: 